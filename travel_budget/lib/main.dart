import 'package:flutter/material.dart';
import 'package:travel_budget/screens/home_screen.dart';
import 'package:travel_budget/screens/landing_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Travel Budget App',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      // home: Home(),
      home: LandingPage(),
      routes: <String, WidgetBuilder>{
        '/auth/signup': (ctx) => Home(),
        '/home': (ctx) => Home(),
      },
    );
  }
}
