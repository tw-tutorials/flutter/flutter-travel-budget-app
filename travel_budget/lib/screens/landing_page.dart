import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:travel_budget/widgets/custom_dialog.dart';

class LandingPage extends StatelessWidget {
  // final primaryColor = const Color(0xFF75A2EA);
  final primaryColor = Colors.blue;

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        width: _width,
        height: _height,
        color: primaryColor,
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                SizedBox(height: _height * .1),
                Text(
                  "Welcome",
                  style: TextStyle(
                    fontSize: 44,
                    color: Colors.white,
                  ),
                ),
                SizedBox(height: _height * .1),
                AutoSizeText(
                  "Let's start planning your next trip",
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 40,
                    color: Colors.white,
                  ),
                ),
                SizedBox(height: _height * .15),
                RaisedButton(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 30),
                    child: Text(
                      'Get Started',
                      style: TextStyle(
                        color: primaryColor,
                        fontSize: 28,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) => CustomDialog(
                        title: 'Would you like to create a free account?',
                        description:
                            'With an account, your data will be securely saved, allowing you to access it from multiple devices.',
                        primaryButtonText: 'Create My Account',
                        primaryButtonRoute: '/auth/signup',
                        secondaryButtonText: 'Maybe Later',
                        secondaryButtonRoute: '/home',
                      ),
                    );
                  },
                ),
                SizedBox(height: _height * .05),
                FlatButton(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 10,
                      horizontal: 30,
                    ),
                    child: Text(
                      "Sign In",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                      ),
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
