import 'package:flutter/material.dart';
import 'package:travel_budget/models/trip.dart';
import 'package:travel_budget/screens/home/home_page.dart';
import 'package:travel_budget/screens/home/explore_page.dart';
import 'package:travel_budget/screens/home/past_trips_page.dart';
import 'package:travel_budget/screens/new_trip/location.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;
  final List<Widget> _pages = [
    HomePage(),
    ExplorePage(),
    PastTripsPage(),
  ];

  void onChangePage(int value) {
    setState(() {
      _currentIndex = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    final Trip newTrip = new Trip(null);

    return Scaffold(
      appBar: AppBar(
        title: Text('Travel Budget App'),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => NewTripLocationScreen(trip: newTrip),
              ));
            },
          )
        ],
      ),
      body: _pages[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: onChangePage,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.explore),
            title: Text('Explore'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.history),
            title: Text('Past Trips'),
          ),
        ],
      ),
    );
  }
}
