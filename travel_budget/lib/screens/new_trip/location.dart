import 'package:flutter/material.dart';
import 'package:travel_budget/models/trip.dart';
import 'package:travel_budget/screens/new_trip/date.dart';

class NewTripLocationScreen extends StatelessWidget {
  final Trip trip;

  NewTripLocationScreen({Key key, @required this.trip}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController _titleController =
        new TextEditingController(text: trip.title);

    return Scaffold(
      appBar: AppBar(
        title: Text('Create Trip - Location'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Enter A Location'),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: TextField(
                controller: _titleController,
                autofocus: true,
              ),
            ),
            RaisedButton(
              child: Text('Continue'),
              color: Theme.of(context).accentColor,
              onPressed: () {
                trip.title = _titleController.text;
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => NewTripDateScreen(trip: trip),
                ));
              },
            )
          ],
        ),
      ),
    );
  }
}
