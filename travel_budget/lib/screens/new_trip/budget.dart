import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:travel_budget/models/trip.dart';

class NewTripBudgetScreen extends StatelessWidget {
  final db = Firestore.instance;

  final Trip trip;

  NewTripBudgetScreen({Key key, @required this.trip}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create Trip - Budget'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Finish'),
            Text('Location ${trip.title}'),
            Text('Start Date ${trip.startDate.toString()}'),
            Text('End Date ${trip.endDate.toString()}'),
            RaisedButton(
              child: Text('Finish'),
              color: Theme.of(context).accentColor,
              onPressed: () async {
                // save data to firebase
                await db.collection('trips').add(trip.toJson());

                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            )
          ],
        ),
      ),
    );
  }
}
