import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:travel_budget/models/Trip.dart';

class HomePage extends StatelessWidget {
  final List<Trip> tripsList = [
    Trip(
      'New York',
      startDate: DateTime.now(),
      endDate: DateTime.now(),
      budget: 200.0,
      travelType: 'car',
    ),
    Trip(
      'Boston',
      startDate: DateTime.now(),
      endDate: DateTime.now(),
      budget: 450.0,
      travelType: 'plane',
    ),
    Trip(
      'Washington D.C.',
      startDate: DateTime.now(),
      endDate: DateTime.now(),
      budget: 900.0,
      travelType: 'bus',
    ),
    Trip(
      'Austin',
      startDate: DateTime.now(),
      endDate: DateTime.now(),
      budget: 170.0,
      travelType: 'car',
    ),
    Trip(
      'Scranton',
      startDate: DateTime.now(),
      endDate: DateTime.now(),
      budget: 180.0,
      travelType: 'car',
    ),
    Trip(
      'Florence',
      startDate: DateTime.now(),
      endDate: DateTime.now(),
      budget: 180.0,
      travelType: 'car',
    )
  ];

  Widget buildTripCard(BuildContext context, int index) {
    final Trip trip = tripsList[index];
    final String startDate = DateFormat('dd/MM/yyyy').format(trip.startDate);
    final String endDate = DateFormat('dd/MM/yyyy').format(trip.endDate);
    final String budget = trip.budget.toStringAsFixed(2);

    return Container(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    trip.title,
                    style: TextStyle(
                      fontSize: 30,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Row(
                children: [
                  Text('$startDate - $endDate'),
                ],
              ),
              SizedBox(height: 88),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '$budget \$',
                    style: TextStyle(fontSize: 35),
                  ),
                  Icon(Icons.directions_car),
                  // Text(trip.travelType),
                ],
              ),
              SizedBox(height: 8),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: tripsList.length,
        itemBuilder: buildTripCard,
      ),
    );
  }
}
